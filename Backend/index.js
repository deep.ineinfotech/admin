const express = require("express");
const sequelize = require("./sequelize");
const userRoutes = require("./Routes/userRoutes");
const cors = require('cors');
const socialLinkRoutes = require('./Routes/socialLinkRoutes');
const testimonialsRoutes = require("./Routes/testimonials");
const mastheadRoutes = require("./Routes/desktopmastheadRoutes");
const mobilemastheadRoutes = require("./Routes/mobilemastheadRoutes");

const app = express();
const PORT = process.env.PORT || 3003;

app.use(express.json()); // Ensure this line is present
app.use(cors())
// Sync the database and start the server
sequelize
  .sync()
  .then(() => {
    app.listen(PORT, () => {
      console.log(`Server is running on http://localhost:${PORT}`);
    });
  })
  .catch((err) => console.error("Error syncing database:", err));

// Register routes
app.use("/users", userRoutes);

// Social routes
app.use("/social",socialLinkRoutes )

// testimonials routes
app.use("/testimonial",testimonialsRoutes)

// Masthead routes
app.use("/desktop_masthead",mastheadRoutes)

//  Mobile Masthead routes
app.use("/mobile_masthead", mobilemastheadRoutes)