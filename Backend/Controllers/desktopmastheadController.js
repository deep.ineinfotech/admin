const desktopMastheads = require("../Models/desktopmasthead");
const { Op } = require("sequelize");

// Create a new masthead
exports.createMasthead = async (req, res) => {
  const { title, description, button_name, button_link, redirection } =
    req.body;

    // Validate required fields
   if (!title || !description || !button_link || !button_name) {
    return res.status(400).json({
      error: "Title, Description, Button name, Button link and redirection field are required.",
      status: false,
    });
  }
  
  try {
    const newMasthead = await desktopMastheads.create({
      title,
      description,
      button_name,
      button_link,
      redirection,
    });
    res.json({
      data: newMasthead,
      message: "Record successfully added",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Get all mastheads
exports.getAllMastheads = async (req, res) => {
  try {
    const { count, rows:mastheads} = await desktopMastheads.findAndCountAll({where: {
      status: {
        [Op.not]: 2, // Exclude records with status 2
      },
    },
    order: [['id', 'DESC']]
  });
    res.json({
      data: mastheads,
      count,
      message: "Record successfully fetched",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Get a masthead by ID
exports.getMastheadById = async (req, res) => {
  const { id } = req.params;
  try {
    const masthead = await desktopMastheads.findByPk(id);
    if (!masthead) {
      return res
        .status(404)
        .json({ error: "Record not found", status: false });
    }
    res.json({
      data: masthead,
      message: "Record successfully fetched",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Update Mastheads
exports.updateMasthead = async (req, res) => {
    const { id } = req.params;
    const { title, description, button_name, button_link, redirection } = req.body;
    try {
      const MastheadToUpdate = await desktopMastheads.findByPk(id);
      if (!MastheadToUpdate) {
        return res
          .status(404)
          .json({ error: "Record not found", status: false });
      }
  
      await MastheadToUpdate.update({ title, description, button_name, button_link, redirection });
      res.json({
        data: MastheadToUpdate,
        message: "Record Successfully Updated",
        status: true,
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error", status: false });
    }
  };

// Delete a masthead by ID
exports.deleteMasthead = async (req, res) => {
  const { id } = req.params;
  try {
    const mastheadToDelete = await desktopMastheads.findByPk(id);
    if (!mastheadToDelete) {
      return res
        .status(404)
        .json({ error: "Record not found", status: false });
    }

    mastheadToDelete.status = 2
     await mastheadToDelete.save();
    // await mastheadToDelete.destroy()
    res.json({
      message: "Record  successfully deleted",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};
