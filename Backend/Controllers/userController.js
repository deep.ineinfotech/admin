// controllers/userController.js

const User = require("../Models/user");
const nodemailer = require("nodemailer");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { use } = require("../Routes/userRoutes");

// New Registration For NewUser
async function registerUser(req, res) {
  const { username, email, password, name } = req.body;

  // Validate required fields
  if (!username || !email || !password || !name) {
    return res.status(400).json({
      error: "All fields (username, email, password, name) are required",
      status: false,
    });
  }

  // Validate password complexity
  const passwordRegex =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
  if (!passwordRegex.test(password)) {
    return res.status(400).json({
      error:
        "Password must contain at least 8 characters, including at least one uppercase letter, one lowercase letter, one digit, and one special character.",
    });
  }

  try {
    // Check if the username already exists
    const existingUser = await User.findOne({
      where: { username: username.trim() },
    });
    console.log("existing User", existingUser);
    if (existingUser) {
      return res.status(400).json({
        error: "Username is already taken. Please choose a different username.",
        status: false,
      });
    }

    // Check email is already exists
    const existingEmail = await User.findOne({ where: { email } });
    if (existingEmail) {
      return res.status(400).json({
        error: "Email is already registered. Please use a different email.",
        status: false,
      });
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    const newUser = await User.create({
      username,
      email,
      password: hashedPassword,
      name,
    });

    res.status(201).json({
      data: newUser,
      message: "Register Successfully",
      status: true,
    });
  } catch (error) {
    console.error("Error creating user:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
}

// Login a registered user
async function loginUser(req, res) {
  const { username, password } = req.body;
  console.log("username", req.body);
  // Validate required fields
  if (!username || !password) {
    return res.status(400).json({
      error: "All fields (username, password) are required",
      status: false,
    });
  }

  try {
    const user = await User.findOne({ where: { username } });
    console.log("userdatavaluessss", user.dataValues);

    if (!user.dataValues) {
      return res.status(401).json({ error: "Invalid credentials" });
    }

    // Compare the provided password with the stored hashed password

    const isPasswordValid = await bcrypt.compare(
      password,
      user.dataValues.password
    );
    console.log("is Password Valid", isPasswordValid);
    if (isPasswordValid) {
      const token = await user.generateAuthToken();
      res.status(200).json({
        data: user,
        message: "login Successfully",
        // token: token,
        status: true,
      });
    } else {
      res.status(401).json({ error: "Invalid credentials", status: false });
    }
  } catch (error) {
    console.error("Error during login:", error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
}

// Forgot password
async function forgotPassword(req, res) {
  const { email } = req.body;

  // Validate email
  if (!email) {
    return res.status(400).json({
      error: "Email is required",
      status: false,
    });
  }

  try {
    const user = await User.findOne({ where: { email } });

    if (!user) {
      return res.status(404).json({ error: "Email not found", status: false });
    }
    const otp = Math.floor(100000 + Math.random() * 900000).toString();
    await user.update({ otp });
    console.log(`Generated OTP for ${user.email}: ${otp}`);

    // Send the OTP to the user's email
    const transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      secure: false,
      auth: {
        user: "deep.ineinfotech07@gmail.com",
        pass: "qqsy jzbj cyot lrep",
      },
    });

    const mailOptions = {
      from: "deep.ineinfotech07@gmail.com", // Update with your Gmail email address
      to: email,
      subject: "OTP for Password Reset",
      text: `Your OTP for password reset is: ${otp}`,
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error(error);
        return res.status(500).json({ error: "Internal Server Error" });
      }

      console.log(`Email sent: ${info.response}`);
      res.status(200).json({
        otp: otp,
        message: "OTP sent to your email",
        status: true,
      });
    });
  } catch (error) {
    console.error("Error during forgot password:", error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
}

// VerifyOtp
// VerifyOtp
async function verifyOTP(req, res) {
  const { otp } = req.body;
  console.log("otp", typeof otp);

  // Validate OTP
  if (!otp) {
    return res.status(400).json({
      error: "OTP is required",
      status: false,
    });
  }

  try {
    const user = await User.findOne({ where: { otp } });

    if (!user) {
      return res.status(401).json({ error: "Invalid OTP", status: false });
    }

    // Clear the OTP after successful verification
    await user.update({ otp: null });

    res.status(200).json({ message: "OTP verified successfully", status: true });
  } catch (error) {
    console.error("Error during OTP verification:", error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
}


// Set New Password
async function setNewPassword(req, res) {
  const { newPassword } = req.body;
  console.log("req.body", req.body);

  // Validate email and newPassword
  if (!newPassword) {
    return res.status(400).json({
      error: "new password are required",
      status: false,
    });
  }

  try {
    const user = await User.findOne();

    if (!user) {
      return res.status(404).json({ error: "Email not found", status: false });
    }

    // Hash the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Update the user's password in the database
    await user.update({ password: hashedPassword });

    res
      .status(200)
      .json({ message: "Password updated successfully", status: true });
  } catch (error) {
    console.error("Error updating password:", error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
}

async function updateProfile(req, res) {
  const { name, profilePhoto, contactNo } = req.body;
  const userId = req.body.userId; // Assuming the user ID is provided in the request body

  try {
    // Find the user by ID
    const user = await User.findByPk(userId);
    console.log("userjksdfsf",user)

    if (!user) {
      return res.status(404).json({ error: "User not found", status: false });
    }

    // Update profile information
    user.name = name || user.name;
    user.profilePhoto = profilePhoto || user.profilePhoto;
    user.contactNo = contactNo || user.contactNo;

    // Save the changes to the database
    await user.save();

   // Return a success response with the updated user data
    const updatedUser = await User.findByPk(userId);

    res.status(200).json({ user: updatedUser, message: "Profile updated successfully", status:true  });
  } catch (error) {
    console.error("Error updating profile:", error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
}

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
  verifyOTP,
  setNewPassword,
  updateProfile,
};
