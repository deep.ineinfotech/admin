const Masthead = require("../Models/masthead");

// Create a new masthead
exports.createMasthead = async (req, res) => {
  const { title, description, button_name, button_link, redirection } =
    req.body;
  try {
    const newMasthead = await Masthead.create({
      title,
      description,
      button_name,
      button_link,
      redirection,
    });
    res.json({
      data: newMasthead,
      message: "Masthead added successfully",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Get all mastheads
exports.getAllMastheads = async (req, res) => {
  try {
    const mastheads = await Masthead.findAll();
    res.json({
      data: mastheads,
      message: "Mastheads fetched successfully",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Get a masthead by ID
exports.getMastheadById = async (req, res) => {
  const { id } = req.params;
  try {
    const masthead = await Masthead.findByPk(id);
    if (!masthead) {
      return res
        .status(404)
        .json({ error: "Masthead not found", status: false });
    }
    res.json({
      data: masthead,
      message: "Masthead fetched successfully",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Update Mastheads
// exports.updateMasthead = async (req, res) => {
//   const { id } = req.params;
//   const { title, description, button_name, button_link, redirection } =
//     req.body;
//   try {
//     const mastheadToUpdate = await Masthead.findByPk(
//       id,
//       { title, description, button_name, button_link, redirection },
//       { new: true }
//     );
//     if (!mastheadToUpdate) {
//       return res
//         .status(404)
//         .json({ error: "Masthead not found", status: false });
//     }
//     res.json({
//       data: mastheadToUpdate,
//       message: "Masthead updated successfully",
//       status: true,
//     });
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: "Internal Server Error", status: false });
//   }
// };

exports.updateMasthead = async (req, res) => {
    const { id } = req.params;
    const { title, description, button_name, button_link, redirection } = req.body;
    try {
      const MastheadToUpdate = await Masthead.findByPk(id);
      if (!MastheadToUpdate) {
        return res
          .status(404)
          .json({ error: "Social Link not found", status: false });
      }
  
      await MastheadToUpdate.update({ title, description, button_name, button_link, redirection });
      res.json({
        data: MastheadToUpdate,
        message: "Data Update Successfuly",
        status: true,
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error", status: false });
    }
  };

// Delete a masthead by ID
exports.deleteMasthead = async (req, res) => {
  const { id } = req.params;
  try {
    const mastheadToDelete = await Masthead.findByPk(id);
    if (!mastheadToDelete) {
      return res
        .status(404)
        .json({ error: "Masthead not found", status: false });
    }
    await mastheadToDelete.destroy()
    res.json({
      message: "Masthead deleted successfully",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};
