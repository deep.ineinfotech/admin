const SocialLink = require("../Models/sociallinks");
const { Op } = require("sequelize");

// Add SocialLinks
exports.addSocialLink = async (req, res) => {
  const { name, socialLink } = req.body;

  // Validate required fields
  if (!name || !socialLink) {
    return res.status(400).json({
      error: "Name and Social link field are required.",
      status: false,
    });
  }

  try {
    const newSocialLink = await SocialLink.create({ name, socialLink });
    res.json({
      data: newSocialLink,
      message: "Record successfully added",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Get All SocialLinks
exports.getAllSocialLinks = async (req, res) => {
  try {
    const { count, rows: socialLinks } = await SocialLink.findAndCountAll( {where: {
      status: {
        [Op.not]: 2, // Exclude records with status 2
      },
    },
    order: [['id', 'DESC']]
  })
  
    res.json({
      data: socialLinks,
      count,
      message: "Record Successfully Featched ",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Get SocialLink BY ID
exports.getSocialLinkById = async (req, res) => {
  const { id } = req.params;
  try {
    const socialLink = await SocialLink.findByPk(id);
    if (!socialLink) {
      return res
        .status(404)
        .json({ error: "Record not found", status: false });
    }
    res.json({
      data: socialLink,
      message: "Record successfully fetched ",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Update SocialLink
exports.updateSocialLink = async (req, res) => {
  const { id } = req.params;
  const { name, socialLink } = req.body;
  try {
    const socialLinkToUpdate = await SocialLink.findByPk(id);
    if (!socialLinkToUpdate) {
      return res
        .status(404)
        .json({ error: "Record not found", status: false });
    }

    await socialLinkToUpdate.update({ name, socialLink });
    res.json({
      data: socialLinkToUpdate,
      message: "Record Successfully Updated",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Delete SocialLinks
exports.deleteSocialLink = async (req, res) => {
  const { id } = req.params;
  try {
    const socialLinkToDelete = await SocialLink.findByPk(id);
    if (!socialLinkToDelete) {
      return res
        .status(404)
        .json({ error: "Record not found", status: false });
    }

     socialLinkToDelete.status = 2
     await socialLinkToDelete.save();

    res.json({ message: "Record successfully deleted", status: true });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};
