const { Op } = require("sequelize");
const Testimonials = require("../Models/testimonials");

// Add Testimonials
exports.addTestimonials = async (req, res) => {
  const { username, description, designation } = req.body;

  // Validate required fields
  if (!username || !description || !designation) {
    return res.status(400).json({
      error: "username, description and designation fields are required",
      status: false,
    });
  }

  try {
    const newTestimonials = await Testimonials.create({
      username,
      description,
      designation,
    });
    res.json({
      data: newTestimonials,
      message: "Record successfully added",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Get All Testimonials
exports.getAllTestimonials = async (req, res) => {
  try {
    const { count, rows: testimonials } =
      await Testimonials.findAndCountAll({
        where: {
          status: {
            [Op.not]: 2, // Exclude records with status 2
          },
        },
        order: [['id', 'DESC']]
      });
    res.json({
      data: testimonials,
      count,
      message: "Record Successfully Featched",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Get Testimonials BY ID
exports.getTestimonialsById = async (req, res) => {
  const { id } = req.params;
  try {
    const testimonials = await Testimonials.findByPk(id);
    if (!testimonials) {
      return res.status(404).json({ error: "Record not found", status: false });
    }
    res.json({
      data: testimonials,
      message: "Record successfully fetched",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Update Testimonials
exports.updateTestimonials = async (req, res) => {
  const { id } = req.params;
  const { username, description, designation } = req.body;
  try {
    const testimonialsToUpdate = await Testimonials.findByPk(id);
    if (!testimonialsToUpdate) {
      return res.status(404).json({ error: "record not found", status: false });
    }

    await testimonialsToUpdate.update({ username, description, designation });
    res.json({
      data: testimonialsToUpdate,
      message: "Record Successfully Updated",
      status: true,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};

// Delete Testimonials
exports.deleteTestimonials = async (req, res) => {
  const { id } = req.params;
  try {
    const testimonialsToDelete = await Testimonials.findByPk(id);
    if (!testimonialsToDelete) {
      return res.status(404).json({ error: "Record not found", status: false });
    }

    testimonialsToDelete.status = 2;
    await testimonialsToDelete.save();

    // await testimonialsToDelete.destroy();
    res.json({ message: "Record successfully deleted", status: true });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error", status: false });
  }
};
