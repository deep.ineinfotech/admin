const express = require('express');
const router = express.Router();
const mastheadController = require('../Controllers/desktopmastheadController');

// Create a new masthead
router.post('/create', mastheadController.createMasthead);

// Get all mastheads
router.get('/', mastheadController.getAllMastheads);

// Get by id a masthead
router.get('/:id', mastheadController.getMastheadById);

// update a masthead
router.put('/:id', mastheadController.updateMasthead);

// delete a masthead
router.delete('/:id', mastheadController.deleteMasthead);

module.exports = router;
