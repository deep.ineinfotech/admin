const express = require("express");
const router = express.Router();
const userController = require("../Controllers/userController");
// const nodemailer = require('nodemailer');
const User = require("../Models/user");
const { verifyToken } = require("../Middleware/authMiddleware");
// const authMiddleware = require("../Middlewares/authMiddleware");

// Registration API endpoint
router.post("/register", userController.registerUser);
router.post("/login", userController.loginUser);
router.post("/forgot-password", userController.forgotPassword);
router.post("/verify-otp", userController.verifyOTP);
router.post("/setnewpassword", userController.setNewPassword);
// router.put('/update-profile', verifyToken, userController.updateProfile);
router.patch("/updateprofile", userController.updateProfile);

// Get all users
router.get("/", verifyToken, async (req, res) => {
  try {
    const decodedData = req.decoded;
    const users = await User.findAll({
        where: { id: decodedData.userId },
     
    });

    res.json({
      data: users,      
      message: "Successfully Retrieved User Data",
      status: true,
    });
  } 
  catch (error) {
    console.error(error);
    res.status(500).json({ 
        error: "Internal Server Error", 
        status: false 
    });
  }
});



module.exports = router;
