const express = require('express');
const router = express.Router();
const testimonialsController = require('../Controllers/testimonialsController');

// Add testimonials
router.post('/create', testimonialsController.addTestimonials);

// fetch testimonials
router.get('/', testimonialsController.getAllTestimonials);

// fetch by id
router.get('/:id', testimonialsController.getTestimonialsById);

// update testimonials
router.put('/:id', testimonialsController.updateTestimonials);

// delete testimonials
router.delete('/:id', testimonialsController.deleteTestimonials);


module.exports = router;