// routes/socialLinkRoutes.js
const express = require("express");
const router = express.Router();
const socialLinkController = require("../Controllers/socialLinkController");

// Add New
router.post("/create", socialLinkController.addSocialLink);

// Get all SocialLinks
router.get("/", socialLinkController.getAllSocialLinks);

// Get Social Links ById
router.get("/:id", socialLinkController.getSocialLinkById);

// Update Links
router.put("/:id", socialLinkController.updateSocialLink);

// Delete Links
router.delete("/:id", socialLinkController.deleteSocialLink);

module.exports = router;
