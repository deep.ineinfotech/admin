const express = require('express');
const router = express.Router();
const mastheadController = require('../Controllers/mastheadController');

// Get all mastheads
router.get('/mastheads', mastheadController.getAllMastheads);

// Create a new masthead
router.post('/mastheads', mastheadController.createMasthead);

// Get by id a masthead
router.get('/mastheads/:id', mastheadController.getMastheadById);

// update a masthead
router.put('/mastheads/:id', mastheadController.updateMasthead);

// delete a masthead
router.delete('/mastheads/:id', mastheadController.deleteMasthead);
module.exports = router;
