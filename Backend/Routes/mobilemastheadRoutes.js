const express = require('express');
const router = express.Router();
const mobilemastheadController = require('../Controllers/mobilemastheadController');

// Create a new masthead
router.post('/create', mobilemastheadController.createMasthead);

// Get all mastheads
router.get('/', mobilemastheadController.getAllMastheads);

// Get by id a masthead
router.get('/:id', mobilemastheadController.getMastheadById);

// update a masthead
router.put('/:id', mobilemastheadController.updateMasthead);

// delete a masthead
router.delete('/:id', mobilemastheadController.deleteMasthead);

module.exports = router;
