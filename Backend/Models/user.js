const { DataTypes } = require("sequelize");
const sequelize = require("../sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");

const User = sequelize.define("User", {
 
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },

  email: {
    type: DataTypes.STRING,
    allowNull: true,
    unique: true,
    validate: {
      isEmail: true,
    },
  },

  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },

  name: {
    type: DataTypes.STRING,
    allowNull: true,
  },

  jwtToken: {
    type: DataTypes.TEXT,
    allowNull: true,
  },

  otp: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  
  profilePhoto: {
    type: DataTypes.STRING, 
    allowNull:true
  },
  contactNo: {
    type: DataTypes.STRING,
    allowNull:true
  },

});

User.prototype.toJSON = function () {
  const user = { ...this.get() };
  delete user.password;
  return user;
};

User.prototype.generateAuthToken = async function () {
  const token = jwt.sign(
    {
      userId: this.id,
      username: this.username,
      name: this.name,
      email: this.email,
      password: this.password,
    },
    "d1b91b362c6f0cd5636125521afb372d06515fe371e7600e1380f23b9d440756",
    { expiresIn: "1h" }
  );
    // Generate refresh token
    const refreshToken = jwt.sign(
      {
        userId: this.id,
      },
      'your_refresh_token_secret',
      { expiresIn: '7d' } // Set the expiration time for refresh token as per your requirement
    );
  
  this.jwtToken = token;
  this.refreshToken= refreshToken
  await this.save(); // Save the token to the database
  return { token, refreshToken };
};

User.prototype.comparePassword = async function (password) {
  return bcrypt.compare(password, this.password);
};


module.exports = User;
