const { DataTypes } = require("sequelize");
const sequelize = require("../sequelize");

const Testimonials = sequelize.define("ine_testimonials", {
  username: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  designation:{
    type:DataTypes.STRING,
    allowNull: false,
},
status: {
  type: DataTypes.INTEGER,
  allowNull: true,
  defaultValue: 1,
},
});

module.exports = Testimonials;

