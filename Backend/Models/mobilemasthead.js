const { DataTypes } = require("sequelize");
const sequelize = require("../sequelize");

const mobileMastheads = sequelize.define("ine_mobile_mastheads", {
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  button_name:{
    type:DataTypes.STRING,
    allowNull: false,
},
 button_link:{
    type:DataTypes.STRING,
    allowNull: false,
},
redirection:{
    type:DataTypes.INTEGER,
    allowNull: false,
    defaultValue:1
},
status: {
  type: DataTypes.INTEGER,
  allowNull: true,
  defaultValue: 1,
},
});

module.exports =  mobileMastheads;