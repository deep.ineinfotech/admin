const { DataTypes } = require("sequelize");
const sequelize = require("../sequelize");

const Sociallink = sequelize.define("ine_socials", {
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  socialLink: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  status: {
    type: DataTypes.INTEGER,
    allowNull: true,
    defaultValue: 1,
  },
});

module.exports = Sociallink;
