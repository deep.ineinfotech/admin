const { DataTypes } = require("sequelize");
const sequelize = require("../sequelize");

const desktopMastheads = sequelize.define("ine_desktop_mastheads", {
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  button_name:{
    type:DataTypes.STRING,
    allowNull: false,
},
 button_link:{
    type:DataTypes.STRING,
    allowNull: false,
},
redirection:{
    type:DataTypes.INTEGER,
    allowNull: false,
    defaultValue:1
    
},
status: {
  type: DataTypes.INTEGER,
  allowNull: true,
  defaultValue: 1,
},
});

module.exports = desktopMastheads;
