const { DataTypes } = require("sequelize");
const sequelize = require("../sequelize");

const Masthead = sequelize.define("Masthead", {
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  button_name:{
    type:DataTypes.STRING,
    allowNull: false,
},
 button_link:{
    type:DataTypes.STRING,
    allowNull: false,
},
redirection:{
    type:DataTypes.INTEGER,
    allowNull: false,
    
}
});

module.exports = Masthead;
