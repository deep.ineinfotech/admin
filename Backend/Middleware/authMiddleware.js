const jwt = require("jsonwebtoken");
const { User } = require("../Models/user");

const verifyToken = (token) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, 'd1b91b362c6f0cd5636125521afb372d06515fe371e7600e1380f23b9d440756', (err, decoded) => {
      if (err) {
        if (err.name === 'TokenExpiredError') {
          // Token has expired, handle accordingly (e.g., refresh token)
          reject(new Error('TokenExpiredError: Token has expired'));
        } else {
          // Token verification failed for some other reason
          reject(new Error('Token verification failed'));
        }
      } else {
        // Token is valid
        resolve(decoded);
      }
    });
  });
};

// Example usage
// const token = 'your-new-token-here';

// verifyToken(token)
//   .then((decoded) => {
//     // Token is valid, proceed with the request
//     console.log(decoded);
//   })
//   .catch((err) => {
//     // Handle the error, e.g., refresh the token or log out the user
//     console.error(err.message);
//   });

module.exports = {
  verifyToken,

};
