"use client";

import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";
import axios from "axios";
import { useEffect, useState } from "react";
import { SuccessAlert, UnsuccessfullAlert } from "../ui/alerts/page";

const MobileMasthead = () => {
  const [formData, setFormData] = useState({
    title: "",
    description: "",
    button_name: "",
    button_link: "",
    redirection: "",
  });

  const [successMessage, setSuccessMessage] = useState(false);
  const [error, setError] = useState(false);

  const [mastheads, setMastheads] = useState([]);
  const [isEditing, setIsEditing] = useState(false);

  // fetch masthead
  const fetchMasthead = async () => {
    try {
      const response = await fetch(
        "http://localhost:3003/mobilemasthead/"
      );
      if (response.ok) {
        const data = await response.json();
        setMastheads(data.data);
      } else {
        console.error("Failed to fetch masthead");
      }
    } catch (error) {
      console.error("Error:", error); 
    }
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    if (!formData.title || !formData.description  || !formData.button_name || !formData.button_link || !formData.redirection) {
      setError("All fields are required");
      setTimeout(() => {
        setError(false);
      }, 2000);
      return;
    }

    try {
      let response;

      if (isEditing) {
        // If in edit mode, send a PUT request
        response = await axios.put(
          `http://localhost:3003/mobilemasthead/${formData.id}`,
          formData
        );
      } else {
        // If not in edit mode, send a POST request
        response = await axios.post(
          "http://localhost:3003/mobilemasthead/create",
          formData
        );
      }

      if (response.status === 200) {
        setSuccessMessage("Record successfully added");
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);

        await fetchMasthead();
        setFormData({
          title: "",
          description: "",
          button_name: "",
          button_link: "",
          redirection: "",
        });
      } else {
        setError(true)
        console.error("Failed to add Masthead");
      }
    } catch (error) {
      setError(true)
      console.error("Error:", error);
    }
  };

  const handleEdit = async (id) => {
    try {
      // Fetch masthead by ID
      const response = await axios.get(
        `http://localhost:3003/mobilemasthead/${id}`,
        formData
      );

      if (response.status === 200) {
        const { title, description, button_name, button_link, redirection } =
          response.data.data;

        // Set form data for editing
        setFormData({
          id,
          title,
          description,
          button_name,
          button_link,
          redirection,
        });
        setIsEditing(true);
      } else {
        console.error("Failed to fetch masthead edit");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  // Add handleUpdate function
  const handleUpdate = async () => {
    try {
      const response = await axios.put(
        `http://localhost:3003/mobilemasthead/${id}`,
        formData
      );

      if (response.status === 200) {
        setSuccessMessage("Masthead updated successfully");
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);

        // Fetch updated list of masthead after update
        await fetchMasthead();
        setFormData({
          title: "",
          description: "",
          button_name: "",
          button_link: "",
          redirection: "",
        });
        // Clear form data after successful update
        setIsEditing(false);
      } else {
        console.error("Failed to update masthead");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  // handle Delete For Masthead
  const handleDelete = async (id) => {
    try {
      const isConfirmed = window.confirm(
        "Are you sure you want to delete this Mobile Masthead?"
      );

      if (!isConfirmed) {
        return;
      }
      const response = await axios.delete(
        `http://localhost:3003/mobilemasthead/${id}`
      );

      if (response.status === 200) {
        setSuccessMessage("Masthead deleted successfully");
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);

        // Fetch updated list of masthead after deletion
        await fetchMasthead();
      } else {
        console.error("Failed to delete masthead");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  useEffect(() => {
    fetchMasthead();
  }, []);

  const mastheadRows = mastheads.map((masthead, index) => (
    <div
      key={masthead.id}
      className="grid grid-cols-3 rounded-sm bg-gray-2 dark:bg-meta-4"
    >
      <div className="p-2.5 xl:p-5">
        <h5 className="text-sm font-medium uppercase xsm:text-base">
          {index + 1}
        </h5>
      </div>
      <div className="p-2.5 xl:p-5">
        <h5 className="text-sm font-medium uppercase xsm:text-base">
          {masthead.title}
        </h5>
      </div>

      <div className="p-2.5 xl:p-5 flex space-x-4">
        <button
          onClick={() => handleEdit(masthead.id)}
          className="flex justify-center rounded bg-primary p-3 font-medium text-gray"
        >
          Edit
        </button>
        <button
          onClick={() => handleDelete(masthead.id)}
          className="flex justify-center rounded bg-primary p-3 font-medium text-gray"
        >
          Delete
        </button>
      </div>
    </div>
  ));

  return (
    <>
      <Breadcrumb pageName="Mobile Masthead" />

      <div className="grid grid-cols-1 gap-9 sm:grid-cols-2">
        <div className="flex flex-col gap-9">
          {/* <!-- Desktop Masthead Form --> */}
          <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
            <div className="border-b border-stroke py-4 px-6.5 dark:border-strokedark">
              <h3 className="font-medium text-black dark:text-white">
                Mobile Masthead
              </h3>
            </div>
            <form onSubmit={handleFormSubmit}>
              {successMessage && <SuccessAlert message={successMessage} />}
              {error && <UnsuccessfullAlert message={error} />}

              <div className="p-6.5">
                <div className="mb-4.5">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Title
                  </label>
                  <input
                    type="text"
                    value={formData.title}
                    onChange={(e) =>
                      setFormData({ ...formData, title: e.target.value })
                    }
                    placeholder="Enter your title"
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  />
                </div>

                <div className="mb-6">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Description
                  </label>
                  <textarea
                    rows={3}
                    placeholder="Type your Description"
                    value={formData.description}
                    onChange={(e) =>
                      setFormData({ ...formData, description: e.target.value })
                    }
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  ></textarea>
                </div>

                <div className="mb-4.5">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Button Name
                  </label>
                  <input
                    type="text"
                    value={formData.button_name}
                    onChange={(e) =>
                      setFormData({ ...formData, button_name: e.target.value })
                    }
                    placeholder="Enter Button Name"
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  />
                </div>
                <div className="mb-4.5">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Button Url
                  </label>
                  <input
                    type="text"
                    value={formData.button_link}
                    onChange={(e) =>
                      setFormData({ ...formData, button_link: e.target.value })
                    }
                    placeholder="Enter Button URL"
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  />
                </div>
                <div className="mb-4.5">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Redirection
                  </label>
                  <div className="relative z-20 bg-transparent dark:bg-form-input">
                    <select className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-5 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary">
                      <option
                        value={formData.redirection}
                        onChange={(e) =>
                          setFormData({
                            ...formData,
                            redirection: e.target.value,
                          })
                        }
                      >
                        1
                      </option>
                      <option
                        value={formData.redirection}
                        onChange={(e) =>
                          setFormData({
                            ...formData,
                            redirection: e.target.value,
                          })
                        }
                      >
                        2
                      </option>
                    </select>
                    <span className="absolute top-1/2 right-4 z-30 -translate-y-1/2">
                      <svg
                        className="fill-current"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g opacity="0.8">
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                            fill=""
                          ></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                </div>

                <button
                  className="flex w-full justify-center rounded bg-primary p-3 font-medium text-gray"
                  type="submit"
                >
                  {isEditing ? "Update Data" : "Add Data"}
                </button>
              </div>
            </form>
          </div>
        </div>

        <div className="flex flex-col gap-9">
          {/* <!-- Mobile Masthead Table --> */}
          <div className="rounded-sm border  border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
            <div className="border-b border-stroke py-4 px-6.5 dark:border-strokedark">
              <h4 className="mb-6 text-xl font-semibold text-black dark:text-white">
                Mobile Masthead
              </h4>
            </div>
            <div className="rounded-sm border border-stroke bg-white px-5 pt-6 pb-2.5 shadow-default dark:border-strokedark dark:bg-boxdark sm:px-7.5 xl:pb-1">
              <div className="flex flex-col">
                <div className="grid grid-cols-4 rounded-sm bg-gray-2 dark:bg-meta-4 sm:grid-cols-3">
                  <div className="p-2.5 xl:p-5">
                    <h5 className="text-sm font-medium uppercase xsm:text-base">
                      Sr.
                    </h5>
                  </div>
                  <div className="p-2.5 xl:p-5">
                    <h5 className="text-sm font-medium uppercase xsm:text-base">
                      Title
                    </h5>
                  </div>
                  {/* <div className="p-6 text-center xl:p-5">
                    <h5 className="text-sm font-medium uppercase xsm:text-base">
                      Link
                    </h5>
                  </div> */}
                  <div className="p-2.5 xl:p-5">
                    <h5 className="text-sm font-medium uppercase xsm:text-base">
                      Action
                    </h5>
                  </div>
                </div>
                {mastheadRows}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MobileMasthead;
