"use client";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";
import axios from "axios";
// import { Metadata } from "next";
import { useEffect, useState } from "react";
import { SuccessAlert, UnsuccessfullAlert } from "../ui/alerts/page";
// export const metadata: Metadata = {
//   title: "Social Link Page | Next.js E-commerce Dashboard Template",
//   description: "This is Social Link page for TailAdmin Next.js",
//   // other metadata
// };

const SocialLinks = () => {
  const [formData, setFormData] = useState({
    name: "",
    socialLink: "",
  });

  const [successMessage, setSuccessMessage] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);
  const [socialLinks, setSocialLinks] = useState([]);
  const [isEditing, setIsEditing] = useState(false);

  // get all social Data
  const fetchSocialLinks = async () => {
    try {
      const response = await fetch("http://localhost:3003/social/");
      if (response.ok) {
        const data = await response.json();
        setSocialLinks(data.data);
      } else {
        console.error("Failed to fetch social links");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  //Submit Data
  const handleFormSubmit = async (e) => {
    e.preventDefault();

    try {
      let response;

      if (isEditing) {
        // If in edit mode, send a PUT request
        response = await axios.put(
          `http://localhost:3003/social/${formData.id}`,
          formData
        );
      } else {
        // If not in edit mode, send a POST request
        response = await axios.post(
          "http://localhost:3003/social/create",
          formData
        );
      }
      setIsEditing(false);
      if (response.status === 200) {
        setSuccessMessage(response.data.message);
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);

        await fetchSocialLinks();
        setFormData({ name: "", socialLink: "" });
      } else {
        console.error("Failed to add social link");
        setErrorMessage(response.data.error);
        setTimeout(() => {
          setErrorMessage(false);
        }, 3000);
      }
    } catch (error) {
      console.error("Error:", error);
      setErrorMessage(error.response.data.error);
      setTimeout(() => {
        setErrorMessage(false);
      }, 3000);
    }
  };

  // Edit Data
  const handleEdit = async (id) => {
    try {
      // Fetch social link by ID
      const response = await axios.get(
        `http://localhost:3003/social/${id}`,
        formData
      );

      if (response.status === 200) {
        const { name, socialLink } = response.data.data;

        // Set form data for editing
        setFormData({ id, name, socialLink });

        setSuccessMessage(response.data.message);
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);
        setIsEditing(true);
      } else {
        console.error("Failed to fetch social link for editing");
        setErrorMessage(response.data.error);
        setTimeout(() => {
          setErrorMessage(false);
        }, 3000);
      }
    } catch (error) {
      console.error("Error:", error);
      setErrorMessage(error.response.data.error);
      setTimeout(() => {
        setErrorMessage(false);
      }, 3000);
    }
  };

  // Delete Data
  const handleDelete = async (id) => {
    try {
      const isConfirmed = window.confirm(
        "Are you sure you want to delete this Social Links?"
      );
      if (!isConfirmed) {
        return;
      }

      const response = await axios.delete(`http://localhost:3003/social/${id}`);

      if (response.status === 200) {
        setSuccessMessage(response.data.message);
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);

        // Fetch updated list of social links after deletion
        await fetchSocialLinks();
      } else {
        console.error("Failed to delete social link");
      }
    } catch (error) {
      console.error("Error:", error);
      setErrorMessage(error.response.data.error);
      setTimeout(() => {
        setErrorMessage(false);
      }, 3000);
    }
  };

  // Add handleUpdate function
  const handleUpdate = async () => {
    try {
      const response = await axios.put(
        `http://localhost:3003/sociallinks/sociallinks/${id}`,
        formData
      );

      if (response.status === 200) {
        setSuccessMessage(response.data.message);
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);

        // Fetch updated list of social links after update
        await fetchSocialLinks();
        setFormData({ name: "", socialLink: "" }); // Clear form data after successful update
      } else {
        setErrorMessage(response.data.message);
        console.error("Failed to update social link");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  useEffect(() => {
    fetchSocialLinks();
  }, []);

  const socialLinkRows = socialLinks.map((socialLink, index) => (
    <div
      key={socialLink.id}
      className="grid grid-cols-3 rounded-sm bg-gray-2 dark:bg-meta-4"
    >
      <div className="p-2.5 xl:p-5">
        <h5 className="text-sm font-medium xsm:text-base">{index + 1}</h5>
      </div>
      <div className="p-2.5 xl:p-5">
        <h5 className="text-sm font-medium xsm:text-base">{socialLink.name}</h5>
      </div>
      {/* <div className="p-2.5 xl:p-5">
        <h5 className="text-sm font-medium xsm:text-base">
          {socialLink.socialLink}
        </h5>
      </div> */}
      <div className="p-2.5 xl:p-5 flex space-x-4">
        <button
          onClick={() => handleEdit(socialLink.id)}
          className="flex justify-center rounded bg-primary p-3 font-medium text-gray"
        >
          <img
                          src="\images\icon\icon-edit.svg"
                          alt="Product Icon"
                          width="20"
                          height="20"
                          className="fill-current"
                        />
        </button>
        <button
          onClick={() => handleDelete(socialLink.id)}
          className="flex justify-center rounded bg-primary p-3 font-medium text-gray"
        >
           <img
                          src="\images\icon\icon-delete.svg"
                          alt="Product Icon"
                          width="20"
                          height="20"
                          className="fill-current"
                        />
          
        </button>
      </div>
    </div>
  ));

  return (
    <>
      <Breadcrumb pageName="Social Links" />

      <div className="grid grid-cols-1 gap-9 sm:grid-cols-2">
        <div className="flex flex-col gap-9">
          {/* <!-- Social Links Form --> */}
          <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
            <div className="border-b border-stroke py-4 px-6.5 dark:border-strokedark">
              <h3 className="font-medium text-black dark:text-white">
                Add Social
              </h3>
            </div>
            <form onSubmit={handleFormSubmit}>
              {successMessage && <SuccessAlert message={successMessage} />}
              {errorMessage && <UnsuccessfullAlert message={errorMessage} />}
              <div className="p-6.5">
                <div className="mb-4.5">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Title:
                  </label>
                  <input
                    type="text"
                    value={formData.name}
                    onChange={(e) =>
                      setFormData({ ...formData, name: e.target.value })
                    }
                    placeholder="Enter ..."
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  />
                </div>

                <div className="mb-4.5">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Link:
                  </label>
                  <input
                    type="text"
                    value={formData.socialLink}
                    onChange={(e) =>
                      setFormData({ ...formData, socialLink: e.target.value })
                    }
                    placeholder="Enter ..."
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  />
                </div>

                <button
                  className="flex w-full justify-center rounded bg-primary p-3 font-medium text-gray"
                  type="submit"
                >
                  {isEditing ? "Update" : "Submit"}
                </button>
              </div>
            </form>
          </div>
        </div>

        <div className="flex flex-col gap-9">
          {/* <!-- Social Link Table --> */}
          <div className="rounded-sm border  border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
            <div className="border-b border-stroke py-4 px-6.5 dark:border-strokedark">
            <h3 className="font-medium text-black dark:text-white">
                List of Social
              </h3>
            </div>
            <div className="rounded-sm border border-stroke bg-white px-5 pt-6 pb-2.5 shadow-default dark:border-strokedark dark:bg-boxdark sm:px-7.5 xl:pb-1">
              <div className="flex flex-col">
                <div className="grid grid-cols-3 rounded-sm bg-gray-2 dark:bg-meta-4 sm:grid-cols-3">
                  <div className="p-2.5 xl:p-5">
                    <h5 className="text-sm font-medium xsm:text-base">Sr.</h5>
                  </div>
                  <div className="p-2.5 xl:p-5">
                    <h5 className="text-sm font-medium xsm:text-base">
                      App Name
                    </h5>
                  </div>
                  {/* <div className="p-6 text-center xl:p-5">
                    <h5 className="text-sm font-medium xsm:text-base">
                      Link
                    </h5>
                  </div> */}
                  <div className="p-2.5 xl:p-5">
                    <h5 className="text-sm font-medium xsm:text-base">
                      Actions
                    </h5>
                  </div>
                </div>
                {socialLinkRows}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SocialLinks;
