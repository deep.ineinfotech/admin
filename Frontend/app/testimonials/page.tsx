"use client";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";
import axios from "axios";
// import { Metadata } from "next";
import { useEffect, useState } from "react";
import { SuccessAlert, UnsuccessfullAlert } from "../ui/alerts/page";
// export const metadata: Metadata = {
//   title: "Testimonials Page | Next.js E-commerce Dashboard Template",
//   description: "This is Testimonials page for TailAdmin Next.js",
//   // other metadata
// };

const Testimonials = () => {
  const [formData, setFormData] = useState({
    username: "",
    description: "",
    designation: "",
  });

  const [successMessage, setSuccessMessage] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);
  const [testimonials, setTestimonials] = useState([]);
  const [isEditing, setIsEditing] = useState(false);

  // Fetch Testimonials Data
  const fetchTestimonials = async () => {
    try {
      const response = await fetch("http://localhost:3003/testimonial/");
      if (response.ok) {
        const data = await response.json();
        setTestimonials(data.data);
      } else {
        console.error("Failed to fetch Testimonials");
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  // Add New Testimonials Form Data
  const handleFormSubmit = async (e) => {
    e.preventDefault();

    try {
      let response;

      if (isEditing) {
        // If in edit mode, send a PUT request
        response = await axios.put(
          `http://localhost:3003/testimonial/${formData.id}`,
          formData
        );
      } else {
        // If not in edit mode, send a POST request
        response = await axios.post(
          "http://localhost:3003/testimonial/create",
          formData
        );
      }
      setIsEditing(false);

      if (response.status === 200) {
        setSuccessMessage(response.data.message);
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);

        await fetchTestimonials();
        setFormData({ username: "", description: "", designation: "" });
      } else {
        setErrorMessage(response.data.error);
        setTimeout(() => {
          setErrorMessage(false);
        }, 3000);
        console.error("Failed to add testimonials");
      }
    } catch (error) {
      setErrorMessage(error.response.data.error);
      setTimeout(() => {
        setErrorMessage(false);
      }, 3000);
      console.error("Error:", error);
    }
  };

  // Edit Testimonials Data
  const handleEdit = async (id) => {
    try {
      // Fetch testimonials by ID
      const response = await axios.get(
        `http://localhost:3003/testimonial/${id}`,
        formData
      );

      if (response.status === 200) {
        const { username, description, designation } = response.data.data;

        // Set form data for editing
        setFormData({ id, username, description, designation });
        setIsEditing(true);
        setSuccessMessage(response.data.message);
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);
      } else {
        console.error("Failed to fetch testimonials for editing");
      }
    } catch (error) {
      console.error("Error:", error);
      setErrorMessage(error.response.data.error);
      setTimeout(() => {
        setErrorMessage(false);
      }, 3000);
    }
  };

  // Delete Testimonials Data
  const handleDelete = async (id) => {
    try {
      const isConfirmed = window.confirm(
        "Are you sure you want to delete this testimonial?"
      );

      if (!isConfirmed) {
        return;
      }

      const response = await axios.delete(
        `http://localhost:3003/testimonial/${id}`
      );

      if (response.status === 200) {
        setSuccessMessage(response.data.message);
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);

        // Fetch updated list of testimonials after deletion
        await fetchTestimonials();
      } else {
        console.error("Failed to delete testimonials");
        setErrorMessage(response.data.error);
        setTimeout(() => {
          setErrorMessage(false);
        }, 3000);
      }
    } catch (error) {
      console.error("Error:", error);
      setErrorMessage(error.response.data.error);
      setTimeout(() => {
        setErrorMessage(false);
      }, 3000);
    }
  };

  // Add handleUpdate function
  const handleUpdate = async () => {
    try {
      const response = await axios.put(
        `http://localhost:3003/testimonial/${id}`,
        formData
      );

      if (response.status === 200) {
        setSuccessMessage(response.data.message);
        setTimeout(() => {
          setSuccessMessage(false);
        }, 3000);

        // Fetch updated list of testimonials after update
        await fetchTestimonials();
        setFormData({ username: "", description: "", designation: "" }); // Clear form data after successful update
      } else {
        console.error("Failed to update Testimonials");
        setErrorMessage(response.data.error);
        setTimeout(() => {
          setErrorMessage(false);
        }, 3000);
      }
    } catch (error) {
      console.error("Error:", error);
      setErrorMessage(error.response.data.error);
      setTimeout(() => {
        setErrorMessage(false);
      }, 3000);
    }
  };

  useEffect(() => {
    fetchTestimonials();
  }, []);

  const testimonialRows = testimonials.map((testimonial, index) => (
    <div
      key={testimonial.id}
      className="grid grid-cols-4 rounded-sm bg-gray-2 dark:bg-meta-4"
    >
      <div className="p-2.5 xl:p-5">
        <h5 className="text-sm font-medium xsm:text-base">{index + 1}</h5>
      </div>
      <div className="p-2.5 xl:p-5">
        <h5 className="text-sm font-medium xsm:text-base">
          {testimonial.username}
        </h5>
      </div>
      <div className="p-2.5 xl:p-5">
        <h5 className="text-sm font-medium xsm:text-base">
          {testimonial.designation}
        </h5>
      </div>
      <div className="p-2.5 xl:p-5 flex space-x-4">
        <button
          onClick={() => handleEdit(testimonial.id)}
          className="flex justify-center rounded bg-primary p-3 font-medium text-gray"
        >
          <img
                          src="\images\icon\icon-edit.svg"
                          alt="Product Icon"
                          width="20"
                          height="20"
                          className="fill-current"
                        />
          
        </button>
        <button
          onClick={() => handleDelete(testimonial.id)}
          className="flex justify-center rounded bg-primary p-3 font-medium text-gray"
        >
           <img
                          src="\images\icon\icon-delete.svg"
                          alt="Product Icon"
                          width="20"
                          height="20"
                          className="fill-current"
                        />
        </button>
      </div>
    </div>
  ));

  return (
    <>
      <Breadcrumb pageName="Testimonial" />

      <div className="grid grid-cols-1 gap-9 sm:grid-cols-2">
        <div className="flex flex-col gap-9">
          {/* <!-- Testimonial Form --> */}
          <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
            <div className="border-b border-stroke py-4 px-6.5 dark:border-strokedark">
              <h3 className="font-medium text-black dark:text-white">
                Add Testimonial
              </h3>
            </div>
            <form onSubmit={handleFormSubmit}>
              {successMessage && <SuccessAlert message={successMessage} />}
              {errorMessage && <UnsuccessfullAlert message={errorMessage} />}
              <div className="p-6.5">
                <div className="mb-4.5">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Name:
                  </label>
                  <input
                    type="text"
                    value={formData.username}
                    onChange={(e) =>
                      setFormData({ ...formData, username: e.target.value })
                    }
                    placeholder="Enter ..."
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  />
                </div>

                <div className="mb-6">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Description:
                  </label>
                  <textarea
                    rows={3}
                    placeholder="Enter ..."
                    value={formData.description}
                    onChange={(e) =>
                      setFormData({ ...formData, description: e.target.value })
                    }
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  ></textarea>
                </div>
                <div className="mb-4.5">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Designation:
                  </label>
                  <input
                    type="text"
                    value={formData.designation}
                    onChange={(e) =>
                      setFormData({ ...formData, designation: e.target.value })
                    }
                    placeholder="Enter ..."
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  />
                </div>

                <button
                  className="flex w-full justify-center rounded bg-primary p-3 font-medium text-gray"
                  type="submit"
                >
                  {isEditing ? "Update" : "Submit"}
                </button>
              </div>
            </form>
          </div>
        </div>

        <div className="flex flex-col gap-9">
          {/* <!-- testimonial Table --> */}
          <div className="rounded-sm border  border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
            <div className="border-b border-stroke py-4 px-6.5 dark:border-strokedark">
            <h3 className="font-medium text-black dark:text-white">
                List of Testimonial
              </h3>
            </div>
            <div className="rounded-sm border border-stroke bg-white px-5 pt-6 pb-2.5 shadow-default dark:border-strokedark dark:bg-boxdark sm:px-7.5 xl:pb-1">
              <div className="flex flex-col">
                <div className="grid grid-cols-4 rounded-sm bg-gray-2 dark:bg-meta-4 sm:grid-cols-4">
                  <div className="p-2.5 xl:p-5">
                    <h5 className="text-sm font-medium xsm:text-base">Sr.</h5>
                  </div>
                  <div className="p-2.5 xl:p-5">
                    <h5 className="text-sm font-medium xsm:text-base">Name</h5>
                  </div>
                  <div className="p-6 text-center xl:p-5">
                    <h5 className="text-sm font-medium xsm:text-base">
                      Designation
                    </h5>
                  </div>
                  <div className="p-2.5 xl:p-5">
                    <h5 className="text-sm font-medium xsm:text-base">
                      Actions
                    </h5>
                  </div>
                </div>
                {testimonialRows}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Testimonials;
